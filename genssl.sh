#!/bin/bash

set -eu

CLIENT_HOSTNAME="zookeeper1-1.net"
KEYPASS="strong"
TRUSTSTORE=truststore
STOREPASS="strong"

EXT=IP_ADDR

function gen_ca {
    echo "------------------- GEN_CA -------------------"
    openssl req -new -x509 -keyout truststore/ca.key -out truststore/ca.crt -days 3650 -nodes -subj "/C=REGION/ST=ST/L=L/O=ORG/CN=kafka.net"
}

function create_keystore {
    echo "------------------- cert_req -------------------"
    keytool -keystore keystore/$CLIENT_HOSTNAME-keystore.jks -genkey -keyalg RSA  -alias $CLIENT_HOSTNAME -dname "CN=$CLIENT_HOSTNAME" -ext "SAN=IP:$EXT" -storepass $STOREPASS -keypass $KEYPASS -noprompt
}

function cert_req {
    echo "------------------- key_store -------------------"
    keytool -keystore keystore/$CLIENT_HOSTNAME-keystore.jks -alias $CLIENT_HOSTNAME  -certreq -file keystore/$CLIENT_HOSTNAME.cer -storepass $STOREPASS -keypass $KEYPASS
}

function sign_key {
    echo "------------------- sign_key -------------------"
    openssl x509 -req -CA truststore/ca.crt -CAkey truststore/ca.key -in keystore/$CLIENT_HOSTNAME.cer -out keystore/$CLIENT_HOSTNAME-sign.cer -days 3650 -CAcreateserial
}

function iport_ca_to_keystore {
    echo "------------------- import_ca_to_keystore -------------------"
    keytool -keystore keystore/$CLIENT_HOSTNAME-keystore.jks -import -file truststore/ca.crt -alias CARoot -dname "CN=$CLIENT_HOSTNAME" -ext "SAN=IP:$EXT" -storepass $STOREPASS -keypass $KEYPASS -noprompt
}

function iport_ca_to_client_trustore {
    echo "------------------- import_ca_to_trustore -------------------"
    keytool -keystore truststore/$CLIENT_HOSTNAME-trustore.jks -import -file truststore/ca.crt -alias CARoot -dname "CN=$CLIENT_HOSTNAME" -ext "SAN=IP:$EXT" -storepass $STOREPASS -keypass $KEYPASS -noprompt
}

function iport_sign_key_to_keystore {
    keytool -keystore keystore/$CLIENT_HOSTNAME-keystore.jks -import -file keystore/$CLIENT_HOSTNAME-sign.cer -alias $CLIENT_HOSTNAME  -storepass $STOREPASS -keypass $KEYPASS -noprompt
}

function iport_sign_key_to_trustore {
    echo "------------------- import_sign_key_to_trustore -------------------"
#    keytool -keystore truststore/$TRUSTSTORE.jks -import -file truststore/ca.crt -alias CARoot -storepass $STOREPASS -keypass $KEYPASS -noprompt
    keytool -keystore truststore/$TRUSTSTORE.jks -import -file keystore/$CLIENT_HOSTNAME-sign.cer -alias $CLIENT_HOSTNAME -storepass $STOREPASS -keypass $KEYPASS -noprompt
}

function exec_certs {
    echo "------------------- exec_certs -------------------"
#    keytool -importkeystore -srckeystore keystore/$CLIENT_HOSTNAME-keystore.jks -destkeystore keystore/$CLIENT_HOSTNAME-keystore.p12 -deststoretype PKCS12 -deststorepass $STOREPASS -destkeypass $KEYPASS
#    keytool --importkeystore -srckeystore truststore/$CLIENT_HOSTNAME-trustore.jks -destkeystore truststore/$CLIENT_HOSTNAME-trustore.p12 -srcstoretype JKS -deststoretype PKCS12 -deststorepass $STOREPASS
    keytool -importkeystore -srckeystore truststore/$TRUSTSTORE.jks -destkeystore truststore/$TRUSTSTORE.p12 -srcstoretype JKS -deststoretype PKCS12 -deststorepass $STOREPASS
##    openssl pkcs12 -in keystore/$CLIENT_HOSTNAME-keystore.p12  -nokeys -out keystore/$CLIENT_HOSTNAME.crt
##    openssl pkcs12 -in keystore/$CLIENT_HOSTNAME-keystore.p12  -nodes -nocerts -out keystore/$CLIENT_HOSTNAME.key
}

function dell_cert {
    echo "------------------- dell_cert -------------------"
    rm -rf keystore/*.cer
}

function client {
##    gen_ca
#    create_keystore
#    cert_req
#    sign_key
#    iport_ca_to_keystore
#    iport_ca_to_client_trustore
#    iport_sign_key_to_keystore
#    iport_sign_key_to_trustore
    exec_certs
#    dell_cert
}

client
