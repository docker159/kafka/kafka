#!/bin/bash

ZOO_HOSTNAME="zookeeper1-1.net"
KEYPASS="PASSWORD"
STOREPASS="PASSWORD"
TRUSTSTORE=truststore
EXT=IP

#1) Create SSL keystore JKS to store local credentials 
#One keystore should be created for each ZK instance.
keytool -genkeypair -alias $ZOO_HOSTNAME -keyalg RSA -keysize 2048 -dname "cn=$ZOO_HOSTNAME" -ext "SAN=IP:$EXT" -keypass $KEYPASS -keystore server/keystore/$ZOO_HOSTNAME.jks -storepass $STOREPASS

#2) Extract the signed public key (certificate) from keystore
keytool -exportcert -alias $ZOO_HOSTNAME -keystore server/keystore/$ZOO_HOSTNAME.jks -file server/keystore/$ZOO_HOSTNAME.cer -rfc

#3) Create SSL truststore JKS containing certificates of all ZooKeeper instances
keytool -importcert -alias $ZOO_HOSTNAME -file server/keystore/$ZOO_HOSTNAME.cer -keystore server/truststore/$TRUSTSTORE.jks -storepass $STOREPASS
