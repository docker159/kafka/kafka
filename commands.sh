######
##List topics
######
  docker exec -ti kafka1-1 kafka-topics   --bootstrap-server kafka1-1:9092,kafka1-2:9092,kafka1-3:9092   --list
######
##View topic insert data
######
  docker exec -ti kafka1-1 kafka-console-consumer --bootstrap-server kafka1-1:9092,kafka1-2:9092,kafka1-3:9092 --topic kafka1-1
######
##Delete topic
######
  docker exec -ti kafka1-1 kafka-topics -bootstrap-server=kafka1-1:9092 --delete --topic user
######
##Add connectors to connect
######
  curl -X POST -H "Accept:application/json" -H "Content-Type:application/json" http://kafka-connector1-1:8083/connectors -d @connect-mysql-broker-sync.json
######
##View all connectors
######
  curl -X GET http://kafka-connector1-1:8083/connectors | jq
######
##View status connector
######
  curl -X GET http://kafka-connector1-1:8083/connectors/!!!CONNECTOR!!!/status | jq
######
##Delet connector
######
  curl -X DELETE -H "Accept:application/json" -H "Content-Type:application/json" http://kafka-connector1-2:8083/connectors/!!!CONNECTOR!!!
